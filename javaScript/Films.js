import { Request } from "./Request.js";
import { CreateElementHTML } from "./CreateElementHTML.js";

export class Films {
    constructor(url) {
        this.url = url;
    }
    getRequest(url) {
        return new Request(url).get();
    }

    renderFilms() {
        return this.getRequest(this.url).then(films => {
            return films.map(({episodeId, name, openingCrawl, characters}) => {
                const container = new CreateElementHTML("div", 'film-wrapper',).element;
                const h2 = new CreateElementHTML("h2", 'title', `Episode ${episodeId}: ${name}`).element;
                const openingCrawlP = new CreateElementHTML("p", 'openingCrawl', `${openingCrawl}`).element;
                const loader = new CreateElementHTML("div", 'loader',).element;
                container.append(h2, loader, openingCrawlP, );

                this.renderCharacters(characters).then(ol => {
                    container.append(ol);

                    if (ol.children.length === characters.length) {
                        loader.remove();
                        h2.style.marginBottom += '42px';
                        openingCrawlP.style.marginBottom = '12px';
                    }
                });

                return container
            })
        }).catch(error => {
            console.error('Load films data error:', error);
        });
    }
    renderCharacters(characters) {
        const characterPromises = characters.map((characterURL) => {
            return this.getRequest(characterURL);
        });

        try {
            return Promise.all(characterPromises).then((characters) => {
                const li = characters.map(({ name }) => {
                    return new CreateElementHTML("li", "listItem", name).element;
                });

                const ol = new CreateElementHTML("ol", "charactersList").element;
                ol.append(...li);

                return ol;
            }).catch(error => {
                console.error('Load characters data error:', error);
            });
        } catch (error) {
            console.error(error);
        }
    }

}
