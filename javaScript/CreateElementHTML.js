export class CreateElementHTML {
    constructor(tagName, className = '', textContent = '') {
        this.element = this.createElement(tagName, className, textContent);
    }

    createElement(tagName, className, textContent) {
        const element = document.createElement(tagName);
        element.className = className;
        element.textContent = textContent;
        return element;
    }
}