export class Request {
    constructor(URL) {
        this.url = URL;
    }
    get() {
        return fetch(this.url)
            .then(response => {
                if (!response.ok) {
                    throw new Error(`HTTP error! status: ${response.status}`);
                }
                return response.json();
            })
            .catch(error => {
                console.error(error);
                throw error;
            });
    }
}